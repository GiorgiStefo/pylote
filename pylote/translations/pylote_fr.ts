<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="842"/>
        <source>Select pen width:</source>
        <translation>Choisir l&apos;épaisseur du crayon :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="565"/>
        <source>Solid</source>
        <translation>Traits pleins</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="572"/>
        <source>Dash</source>
        <translation>Tirets</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="579"/>
        <source>Dot</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="586"/>
        <source>Dash Dot</source>
        <translation>Tirets Points</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="593"/>
        <source>Dash Dot Dot</source>
        <translation>Tirets Points Points</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="786"/>
        <source>Select color</source>
        <translation>Choisir une couleur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="696"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="183"/>
        <source>Open...</source>
        <translation>Ouvrir...</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="223"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="191"/>
        <source>Reload</source>
        <translation>Recharger</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="199"/>
        <source>Next file</source>
        <translation>Fichier suivant</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="264"/>
        <source>Configure</source>
        <translation>Configurer</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="272"/>
        <source>Minimize</source>
        <translation>Minimiser</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="703"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="89"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="391"/>
        <source>Lock instruments</source>
        <translation>Bloquer les instruments</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="430"/>
        <source>Show false cursor</source>
        <translation>Afficher le faux curseur</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="289"/>
        <source>New screenshot</source>
        <translation>Nouvelle photo d&apos;écran</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="303"/>
        <source>White page</source>
        <translation>Page blanche</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="310"/>
        <source>Dotted paper</source>
        <translation>Papier pointé</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="317"/>
        <source>Grid</source>
        <translation>Grille</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="324"/>
        <source>Choose background</source>
        <translation>Choisir le fond</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="333"/>
        <source>Ruler</source>
        <translation>Règle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="352"/>
        <source>Square</source>
        <translation>Équerre</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="371"/>
        <source>Protractor</source>
        <translation>Rapporteur</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="381"/>
        <source>Compass</source>
        <translation>Compas</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="704"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="450"/>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="457"/>
        <source>Curve</source>
        <translation>Courbe</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="464"/>
        <source>Highlighter</source>
        <translation>Surligneur</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="471"/>
        <source>Add text</source>
        <translation>Ajouter un texte</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="478"/>
        <source>Add point</source>
        <translation>Ajouter un point</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="485"/>
        <source>Add pixmap</source>
        <translation>Ajouter une image</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="74"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="769"/>
        <source>Styles</source>
        <translation>Styles</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="68"/>
        <source>Font</source>
        <translation>Police d&apos;écriture</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="520"/>
        <source>Delete selected object</source>
        <translation>Supprimer l&apos;objet sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="527"/>
        <source>Erase all</source>
        <translation>Effacer tout</translation>
    </message>
    <message>
        <location filename="../libs/utils_instruments.py" line="277"/>
        <source>Point name:</source>
        <translation>Nom du point :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="410"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="447"/>
        <source>Other</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="115"/>
        <source>&lt;P&gt;&lt;/P&gt;&lt;P ALIGN=LEFT&gt;Here you can select on which screen you will to work.&lt;/P&gt;</source>
        <translation>&lt;P&gt;&lt;/P&gt;&lt;P ALIGN=LEFT&gt;Ici vous pouvez choisir sur quel écran travailler.&lt;/P&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="62"/>
        <source>Screen usage mode</source>
        <translation>Mode d&apos;utilisation de l&apos;écran</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="66"/>
        <source>All space</source>
        <translation>Tout l&apos;espace</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="68"/>
        <source>Full screen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="96"/>
        <source>Screen number</source>
        <translation>Numéro d&apos;écran</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="423"/>
        <source>Screen</source>
        <translation>Écran</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="215"/>
        <source>Icon size</source>
        <translation>Taille des icônes</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="315"/>
        <source>Enter a value between {0} and {1}:</source>
        <translation>Entrez une valeur entre {0} et {1} :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="106"/>
        <source>Insert text</source>
        <translation>Insérer un texte</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="615"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="236"/>
        <source>Visible actions</source>
        <translation>Actions visibles</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="283"/>
        <source>Screenshot delay</source>
        <translation>Délai des captures d&apos;écran</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="285"/>
        <source>in milliseconds</source>
        <translation>en millisecondes</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="431"/>
        <source>Tools window</source>
        <translation>Boîte à outils</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="439"/>
        <source>Kids</source>
        <translation>Enfants</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="215"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="207"/>
        <source>Previous file</source>
        <translation>Fichier précédent</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="243"/>
        <source>Print</source>
        <translation type="obsolete">Imprimer</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="251"/>
        <source>Export PDF</source>
        <translation type="obsolete">Exporter en PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="250"/>
        <source>Export SVG</source>
        <translation>Exporter en SVG</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="506"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="513"/>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="279"/>
        <source>Create a launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="401"/>
        <source>Lock units</source>
        <translation>Bloquer les unités</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="409"/>
        <source>Save units</source>
        <translation>Enregistrer les unités</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="416"/>
        <source>Restore backed up units</source>
        <translation>Restaurer les unités sauvegardées</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="423"/>
        <source>Reset units</source>
        <translation>Réinitialiser les unités</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="361"/>
        <source>Square (not graduated)</source>
        <translation>Équerre (non graduée)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="333"/>
        <source>Print configuration (and PDF export)</source>
        <translation>Configuration de l&apos;impression (et export PDF)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="339"/>
        <source>Print mode</source>
        <translation>Mode d&apos;impression</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="341"/>
        <source>Full page</source>
        <translation>Page entière</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="343"/>
        <source>True size</source>
        <translation>Taille réelle</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="354"/>
        <source>Orientation</source>
        <translation>Orientation</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="356"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="358"/>
        <source>Landscape</source>
        <translation>Paysage</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="82"/>
        <source>&lt;p align=left&gt;&lt;b&gt;All space: &lt;/b&gt;the application use all the free space on desktop.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Full screen: &lt;/b&gt;choose this if you ave problems with All space mode.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=center&gt;&lt;b&gt;If you change this, you need to restart application.&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p align=left&gt;&lt;b&gt;Tout l&apos;espace : &lt;/b&gt;le logiciel occupe tout l&apos;espace libre du bureau.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Plein écran : &lt;/b&gt;choisir ce mode si vous avez des problèmes avec le mode &quot;Tout l&apos;espace&quot;.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=center&gt;&lt;b&gt;En cas de changement, il faudra redémarrer le logiciel.&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="378"/>
        <source>&lt;p align=left&gt;&lt;b&gt;Full page: &lt;/b&gt;printing will be adapted to the dimensions of the page.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;True size: &lt;/b&gt;the printed document comply with the dimensions of your figures.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</source>
        <translation>&lt;p align=left&gt;&lt;b&gt;Page entière : &lt;/b&gt;l&apos;impression sera adaptée aux dimensions de la page.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Taille réelle : &lt;/b&gt;le document imprimé respectera les dimensions de vos figures.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="891"/>
        <source>Custom color</source>
        <translation>Couleur personnalisée</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="896"/>
        <source>Custom width</source>
        <translation>Taille personnalisée</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="308"/>
        <source>Attach distance</source>
        <translation>Distance de rattachement</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="310"/>
        <source>between lines or points and ruler or square</source>
        <translation>entre les lignes ou points et la règle ou l&apos;équerre</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="499"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="667"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Choisissez le dossier où le lanceur sera créé</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="110"/>
        <source>About {0}</source>
        <translation>À propos de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="70"/>
        <source>(version {0})</source>
        <translation>(version {0})</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="92"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="97"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="728"/>
        <source>Restore</source>
        <translation>Restaurer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="819"/>
        <source>Delete this item ?</source>
        <translation>Supprimer cet objet ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="988"/>
        <source>Save File</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1252"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1300"/>
        <source>not a valid file</source>
        <translation>Ce n&apos;est pas un fichier valide</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1322"/>
        <source>Failed to open
  {0}</source>
        <translation>Impossible d&apos;ouvrir
  {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="526"/>
        <source>Open Image</source>
        <translation>Choisir une image</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="71"/>
        <source>Pylote Files (*.plt)</source>
        <translation>Fichiers Pylote (*.plt)</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="523"/>
        <source>Image Files</source>
        <translation>Fichiers images</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1061"/>
        <source>Failed to save
  {0}</source>
        <translation>Impossible d&apos;enregistrer
  {0}</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1543"/>
        <source>Export as SVG File</source>
        <translation>Exporter en SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1545"/>
        <source>SVG Files (*.svg)</source>
        <translation>Fichiers SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1600"/>
        <source>Export as PDF File</source>
        <translation>Exporter en PDF</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1602"/>
        <source>PDF Files (*.pdf)</source>
        <translation>Fichiers PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="697"/>
        <source>Geometry instruments</source>
        <translation>Instruments de géométrie</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="759"/>
        <source>Basic tools</source>
        <translation>Outils de base</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="761"/>
        <source>Screenshots and backgrounds</source>
        <translation>Photos d&apos;écran et fonds</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="763"/>
        <source>Drawing tools</source>
        <translation>Outils de dessin</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="870"/>
        <source>click to edit</source>
        <translation>cliquez pour modifier</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="765"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="872"/>
        <source>Width:</source>
        <translation>Épaisseur :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="767"/>
        <source>Widths</source>
        <translation>Épaisseurs</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="170"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="492"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1573"/>
        <source>Export as PNG File</source>
        <translation>Exporter en PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1575"/>
        <source>PNG Files (*.png)</source>
        <translation>Fichiers PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="257"/>
        <source>Export PNG</source>
        <translation>Exporter en PNG</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="342"/>
        <source>Ruler (not graduated)</source>
        <translation>Règle (non graduée)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="688"/>
        <source>Recent Files</source>
        <translation>Fichiers récents</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1097"/>
        <source>No name</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1110"/>
        <source>File must be saved ?</source>
        <translation>Faut il enregistrer le fichier ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="175"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="296"/>
        <source>Transparent area</source>
        <translation>Zone transparente</translation>
    </message>
</context>
</TS>
